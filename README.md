# Intergalactic Singles Night

Intergalactic Singles Night is a surreal dating simulator with choices, romance, and fun minigames! It was made for the [Godot Wild Jam 48](https://itch.io/jam/godot-wild-jam-48), and all of the art, programming, and writing was done by me.

## Music Credits
Menu Theme: [Space - Lasercheese](https://opengameart.org/content/space-orchestral)

Exterior Bar: [Ritual - Of Far Different Nature](https://fardifferent.itch.io/escape)

Interior Bar: [Pure - Of Far Different Nature](https://fardifferent.itch.io/escape)

Blackjack: [Crypt - Of Far Different Nature](https://fardifferent.itch.io/escape)

Shooting Gallery: [Control - Of Far Different Nature](https://fardifferent.itch.io/escape)

Dead Man's Tale theme - Original recording on marimba and piano



### I want to play!

Just head over to [the itch page](https://protodrew.itch.io/intergalactic-singles-night) to play or download

### License

The Game's code is licensed under the GPL 3.0 License, while the Art is [Creative Commons Attribution Share-Alike](https://creativecommons.org/licenses/by-sa/4.0/)

Both Godot and the Dialogic plugin used for the game are licensed under the MIT license.

All of the Music used uses either CC BY or CC BY SA, see original download page for details
