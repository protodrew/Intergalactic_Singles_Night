extends Node2D


func _ready():
	get_parent().playmus("menu")


func _on_New_Game_pressed():
	get_parent().move_scene("first_dates")


func _on_Options_pressed():
	$Option_Menu.visible = !$Option_Menu.visible


func _on_Quit_pressed():
	get_tree().quit()


func _on_Logo_pressed():
	OS.shell_open("https://protodrew.website")


func _on_HSlider_value_changed(value):
	get_parent().set_volume(value)
	get_parent().play_sfx("shoot")


func _on_SQL_Minigame_pressed():
	get_parent().move_scene("Shooting_Gallery")


func _on_Angio_Minigame_pressed():
	get_parent().move_scene("Blackjack")


func _on_Scunge_Story_pressed():
	get_parent().move_scene("Dead_Mans_Tale")
