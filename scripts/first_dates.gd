extends Node2D


onready var loveometer := $Loveometer


func change_date(date):
	for x in get_groups():
		remove_from_group(x)
	add_to_group("date " + str(date))
	loveometer.date = date
	loveometer.check_date()
