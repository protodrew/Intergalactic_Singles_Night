extends Sprite


onready var needle := $Needle
onready var date : String = check_date() # cannot type with := because function doesn't infer type

func _ready():
	check_date()

func _process(delta):
	needle.rect_rotation = get_parent().get_parent().compatibility_dict[date] - 90 # positions the needle for the loveometer

func check_date():
	for x in get_parent().get_groups():
		if "date" in x:
			return(x.substr(5,x.length())) # allows us to check which vignette we are in and get the date
