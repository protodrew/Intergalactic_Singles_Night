extends Node2D

const ROUNDLEN := 2600
var round_timer := 2600
var shoot_spacing := 300
var timer := 40
var points := 0
var high_score := 0
var lives := 3
var can_shoot := true
onready var score := $Score
onready var time := $Time
onready var highscoredisp := $High_Score
onready var cursor := $Cursor
onready var gameover := $Game_Over

var cycles := 0

func _ready():
	get_parent().playmus("shooting_gallery")

func _process(delta):
	
	if lives <= 0:
		end_game()
	
	if cursor.canshoot:
		round_timer -= 1 * delta
		if timer <= 0:
			cycle()
		if round_timer <= 0:
			end_game()

		timer -= 1
		time.text = "Time: " + str(round_timer / 10)
		score.text = "Score: " + str(points)
		highscoredisp.text = "High Score: " + str(high_score)
	elif Input.is_action_just_pressed("reset_game"):
		reset_game()

func end_game():
	if points > high_score:
		high_score = points
	cursor.canshoot = false
	cursor.visible = false
	for x in get_children():
		if "target" in x.name:
			x.visible = false
	gameover.visible = true
	
func reset_game():
	gameover.visible = false
	lives = 3
	cycles = 0
	points = 0
	round_timer = ROUNDLEN
	timer = 40
	cursor.visible = true
	for x in get_children():
		if "target" in x.name:
			x.frame = 1
			x.visible = true
	cursor.canshoot = true
	

func cycle():
	cycles += 1
	for x in get_children():
		if "target" in x.name:
			x.cycle()
	timer = shoot_spacing - (cycles * 5)

func game_over():
	pass

func shoot(target):
	get_parent().play_sfx("shoot")
	for x in get_children():
		if x.name == "target"+str(target):
			if x.get_frame() == 0:
				points += 100
				x.set_frame(1)
			else:
				lives -= 1
