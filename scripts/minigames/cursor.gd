extends Sprite

var canshoot := true
var positions := [Vector2(136,141),Vector2(316,141),Vector2(496,141),
				 Vector2(136,291),Vector2(316,291),Vector2(496,291),
				 Vector2(136,441),Vector2(316,441),Vector2(496,441)
				]

func _process(_delta):
	var aim := 4 # center
	if canshoot:
		if Input.is_action_pressed("ui_up"):
			if Input.is_action_pressed("ui_right"):
				aim = 2
			elif Input.is_action_pressed("ui_left"):
				aim = 0
			else:
				aim = 1
		elif Input.is_action_pressed("ui_down"):
			if Input.is_action_pressed("ui_right"):
				aim = 8
			elif Input.is_action_pressed("ui_left"):
				aim = 6
			else:
				aim = 7
		elif Input.is_action_pressed("ui_left"):
			aim = 3
		elif Input.is_action_pressed("ui_right"):
			aim = 5
		
		position = positions[aim]
		
		if Input.is_action_just_pressed("shoot"):
			get_parent().shoot(aim)
			print(aim)
