extends Node2D

var random = RandomNumberGenerator.new()
var player_hand := []
var dealer_hand := []
var turn := "player"
var wins := 0
onready var wincount = $Wins

func _ready():
	get_parent().playmus("blackjack")
	deal_hand()

func reset_game():
	for x in get_children():
		if !x.is_in_group("persist"):
			remove_child(x)
	player_hand = []
	dealer_hand = []
	turn = "player"
	deal_hand()

func _process(delta):
	wincount.text = "Wins: " + str(wins)
	if turn == "player":
		if Input.is_action_just_pressed("ui_right"):
			deal_card("player")
		elif Input.is_action_just_pressed("ui_left"):
			turn = "dealer"
	elif turn == "dealer":
		if check_hand("dealer") < 17:
			deal_card("dealer")
		else:
			end_game("-1")

func end_game(hand):
	match(hand):
		player_hand:
			wins += 1
		dealer_hand:
			pass
		"-1":
			if check_hand("player") >= check_hand("dealer"):
				wins += 1
			else:
				pass
	reset_game()

func deal_hand():
	deal_card("player")
	deal_card("player")
	deal_card("dealer")
	deal_card("dealer")

func get_card():
	random.randomize()
	var rand_c : int = random.randi_range(0,12) # which card number you have
	var rand_s : int = random.randi_range(0,3) # which suit (not really relevant, just cosmetic)
	var card := Vector2(rand_c,rand_s)
	return card

func draw_card(person):
	var p = load("res://scenes/minigames/Cards.tscn").instance()
	if person == "player":
		p.position = Vector2(50*player_hand.size(),500)
		p.frame_coords = player_hand[player_hand.size()-1]
	elif person == "dealer":
		p.position = Vector2(50*dealer_hand.size(),100)
		p.frame_coords = dealer_hand[dealer_hand.size()-1]
	add_child(p)


func get_value(card : Vector2):
	match int(card.x):
		8,9,10,11:
			return 10
		12:
			return 11
		_:
			return card.x+2

func deal_card(person):
	var card : Vector2 = get_card()
	if person == "dealer":
		dealer_hand.append(card)
		draw_card("dealer")
		check_hand("dealer")
	else:
		player_hand.append(card)
		draw_card("player")
		check_hand("player")

func check_hand(person):
	var total := 0
	var num_aces := 0
	var hand : Array
	var other_hand : Array
	if person == "dealer":
		hand = dealer_hand
		other_hand = player_hand
	else:
		hand = player_hand
		other_hand = dealer_hand
	
	for i in hand:
		var val : int = get_value(i)
		if val == 11:
			num_aces += 1
		else:
			total += val

	while num_aces > 0:
		if total + 11 > 21:
			total += 1
		else:
			total += 11
			
	if total > 21:
		end_game(other_hand)
	elif total == 21:
		end_game(hand)
	else:
		return total
