extends Sprite

var can_decrease := false
var time := 0
var random = RandomNumberGenerator.new()

func _ready():
	frame = 1

func _process(_delta):
	if can_decrease:
		time -= 1
	if time <= 0:
		frame = 1
		can_decrease = false

func cycle():
	random.randomize()
	var val = random.randi_range(0,175)
	print(val)
	if val > 90:
		frame = 0
		can_decrease = true
		time = 150
