extends Node2D

var compatibility_dict := {"angio":0,"sql":0,"scunge":0}
var menu = preload("res://scenes/Menu.tscn")

func _ready():
	add_child(menu.instance())

func _process(_delta):
	if Input.is_action_just_pressed("quit"):
		quit()

func set_volume(value):
	$Music.volume_db = value
	$SFX.volume_db = value / 2

func quit():
	get_tree().quit()

func move_scene(var nextscene):
	var next_vignette = "res://scenes/vignettes/"+nextscene+".tscn" # generates path to scene
	clear() # clears current non-persistent scenes
	add_child(load(next_vignette).instance()) # instances generated path

func change_background(var background):
	for x in get_children():
		if x.is_in_group("bkg"):
			remove_child(x)
	add_child(load("res://scenes/" + background + ".tscn").instance())

func play_sfx(sound):
	sound = load("res://audio/" + sound + ".ogg")
	sound.loop = false;
	$SFX.set_stream(sound) # had to do the $ rather than an onready var due to a bug, lost abt an hour here :/
	$SFX.play()

func playmus(song_name):
	var song = load("res://audio/music/" + str(song_name) + ".ogg")
	$Music.set_stream(song)
	$Music.play()

func clear():
	for child in get_children(): # parses through all children
		if !child.is_in_group("PERSISTENT"): # passes over all of the nodes we don't want to remove
				remove_child(child) # removes the rest 

func modify_compatibility(person, value):
	if person in compatibility_dict: # checks if given person name is valid
		if compatibility_dict[person] + int(value) >= 180: # max value
			compatibility_dict[person] = 180
		elif compatibility_dict[person] + int(value) <= 0: # min value
			compatibility_dict[person] = 0
		else:
			compatibility_dict[person] += int(value)
	else:
		print("invalid person")

func minigame_levels():
	Dialogic.set_variable("angio_compat",compatibility_dict[0])
	Dialogic.set_variable("sql_compat",compatibility_dict[1])
	Dialogic.set_variable("scunge_compat",compatibility_dict[2])
